var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var sequence = require('run-sequence');
var watch = require('gulp-watch');
var typescript = require('gulp-typescript');

var paths = {
    source: 'src/04 typescript/',
    destination: 'dist/'
};

/**
 * Cleans dist folder
 */
gulp.task('clean', function() {
    return gulp
        .src(paths.destination, { read: false })
        .pipe(clean())
});


/**
 * Builds HTML files and moves to dist folder
 */
gulp.task('build-html', function() {
    gulp
        .src(paths.source + '**/*.html')
        .pipe(gulp.dest(paths.destination));
});


/**
 * Builds CSS files and moves to dist folder
 */
gulp.task('build-css', function() {
    gulp
        .src(paths.source + '**/*.css')
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(paths.destination + 'css/'));
});


/**
 * Build TypeScript files and bundle into dist folder
 */
gulp.task('build-js', function() {
    return gulp
        .src(paths.source + '**/*.ts')
        .pipe(typescript({
            target: "ES5",
            module: 'system',
            noImplicitAny: true,
            noImplicitThis: true,
            alwaysStrict: true,
            removeComments: true,
            allowJs: true,
            out: 'js/scripts.js'
        }))
        .pipe(gulp.dest(paths.destination));
});


/** Build the whole application */
gulp.task('build', function(done) {
    sequence('clean',
             'build-html',
             'build-css',
             'build-js',
             done);
});


/**
 * Watch file changes and rebuild the application
 */
gulp.task('watch', ['build'], function() {
   gulp.watch('src/**/*.*', ['build']);
});


