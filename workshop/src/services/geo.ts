/// <reference path="../model/configuration.ts" />

namespace HF.CurrencyConverter {

    /**
     * Geolocation
     */
    export interface GeoLocation {
        latitude: number
        longitude: number
    }


    /**
     * Geo-location services
     */
    export class Geo {

        /**
         * Returns current location using HTML5 GeoLocation API
         */
        static getLocation(configuration: EnvironmentConfiguration): Promise<GeoLocation> {
            return new Promise<GeoLocation>(resolve => {

                if (configuration.name == 'mock') {
                    console.log('Fetching mock geo-location')
                    resolve({
                        latitude: 53.2719916,
                        longitude: -6.201853799999999
                    })
                }

                // Check for API presence, unfortunately we can't provide a polyfill here ...
                if (navigator.geolocation) {
                    console.log('Fetching geo-location')
                    navigator.geolocation.getCurrentPosition(position => {
                        resolve({
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        })
                    });
                }
                else {
                    console.log('GeoLocation API not available')
                    resolve(undefined)
                }
            })
        }


        /**
         * Finds country using the specified location
         */
        static async getCountry(location: GeoLocation, configuration: EnvironmentConfiguration): Promise<string> {
            let country: string
            let url = configuration.endpoints
                        .reverseGeoCode
                        .replace('{latitude}', location.latitude.toFixed(6))
                        .replace('{longitude}', location.longitude.toFixed(6))

            let response = await fetch(url)
            let data = await response.json()
            if (data)
                country = data["countryCode"]
            return country
        }

        /**
         * Finds country currency
         */
        static async getCountryCurrency(countryCode: string, configuration: EnvironmentConfiguration): Promise<string> {
            let currency: string
            let url = configuration.endpoints
                        .countryDetails
                        .replace('{countryCode}', countryCode)
            let response = await fetch(url)
            let data = await response.json()
            if (data && data["currencies"] && data["currencies"].length)
                currency = data["currencies"][0]["code"]
            return currency
        }


    }

}