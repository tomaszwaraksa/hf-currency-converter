namespace HF.CurrencyConverter {

    /**
     * DOM utilities
     */
    export class DOM {

        /**
         * Checks if element has the specified class set
         * */
        static hasClass(element: Element, className: string): boolean {
            let result = false
            if (element && className) {
                result = element.className.split(' ').indexOf(className) > -1
            }
            return result
        }


        /**
         * Sets the specified class on the element
         * Preserves other classes
         * */
        static addClass(element: Element, className: string) : boolean {
            if (element && className && !DOM.hasClass(element, className)) {
                element.className = element.className + ' ' + className
                return true
            }
            return false
        }


        /**
         * Sets the specified class on the element
         * Preserves other classes
         * */
        static removeClass(element: Element, className: string) : boolean {
            if (element && className && DOM.hasClass(element, className)) {
                element.className = element.className.split(' ').filter(name => name != className).join(' ')
                return true
            }
            return false
        }

        /**
         * Hides an element
         */
        static hide(element: Element) {
            DOM.addClass(element, 'hidden')
        }


        /**
         * Displays an element
         */
        static show(element: Element) {
            DOM.removeClass(element, 'hidden')
        }


    }

}