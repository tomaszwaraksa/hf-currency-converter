/// <reference path="../model/configuration.ts" />

namespace HF.CurrencyConverter {

    /**
     * Application configuration provider
     */
    export class ConfigurationProvider {

        /**
         * Fetches application configuration
         */
        static async get(): Promise<EnvironmentConfiguration> {
            let response = await fetch('data/configuration.json', { cache: 'no-cache' })
            let json = await response.json()
            let configuration = <Configuration>json
            return configuration.environments.find(environment => environment.name == configuration.environment)
        }

    }

}
