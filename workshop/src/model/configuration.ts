namespace HF.CurrencyConverter {

    /**
     * Application configuration
     */
    export interface Configuration {
        /**
         * Current environment
         */
        environment: string

        /**
         * All environments
         */
        environments: EnvironmentConfiguration[]
    }


    /**
     * Environment configuration
     */
    export interface EnvironmentConfiguration {
        /**
         * Environment name
         */
        name: string

        /**
         * Endpoint URLs
         */
        endpoints: EndpointsConfiguration
    }


    /**
     * Endpoints configuration
     */
    export interface EndpointsConfiguration {
        /**
         * If true, usage of cached data is allowed
         */
        allowCache: boolean

        /**
         * Endpoint for fetching currencies
         */
        currencies: string

        /**
         * Endpoint for fetching exchange rates
         */
        exchangeRates: string

        /** Endpoint for resolving geo-location into country code */
        reverseGeoCode: string

        /** Endpoint for fetching detailed country information */
        countryDetails: string
    }


}
