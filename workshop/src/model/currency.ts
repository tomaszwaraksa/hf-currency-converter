namespace HF.CurrencyConverter {

    /** Currency */
    export interface Currency {

        /**
         * Currency code
         */
        code: string

        /**
         * Currency name
         */
        name: string

    }

}