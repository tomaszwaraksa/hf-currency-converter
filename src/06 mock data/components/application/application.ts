/// <reference path="../../model/currency.ts" />
/// <reference path="../../model/exchange-rate.ts" />
/// <reference path="../../components/form/form.ts" />
/// <reference path="../../services/api-client.ts" />

namespace HF.CurrencyConverter {

    /**
     * Application controller
     */
    export class Application {

        /**
         * List of supported currencies
         */
        static currencies: Currency[]


        /**
         * List of exchange rates
         */
        static exchangeRates: ExchangeRates


        /**
         * Form controller
         */
        static formController: FormController


        /** Initializes the application */
        static async main() {

            // fetch currencies
            /**
            APIClient
                .getCurrencies()
                .then(currencies => {
                    Application.currencies = currencies
                    console.log('Got currencies', currencies)
                })
             */
            Application.currencies = await APIClient.getCurrencies()
            Application.exchangeRates = await APIClient.getExchangeRates()

            // initialize the input form
            Application.formController = new FormController(Application.currencies, Application.exchangeRates)

            console.log('Ready.')
        }
    }


    // Initialize the application when window has finished rendering
    window.addEventListener('load', () => {
        Application.main()
    })

}