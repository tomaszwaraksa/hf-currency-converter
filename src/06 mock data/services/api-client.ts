/// <reference path="../model/currency.ts" />
/// <reference path="../model/exchange-rate.ts" />

namespace HF.CurrencyConverter {

    export class APIClient {

        /**
         * Retrieves the list of currencies
         */
        static async getCurrencies(): Promise<Currency[]> {
            let url = 'data/currencies.json'

            /*
            return fetch(url)                                   // fetch currencies list from the specified URL
                    .then(response => response.json())          // parse the response as JSON
                    .then(data => {                             // transform to list of currencies
                        let items = Object.keys(data)
                        let currencies : Currency[] = items.map(key => {
                            return {
                                code: key,
                                name: data[key]
                            }
                        })
                        return currencies
                    })
            */
            let response = await fetch(url)
            let data = await response.json()
            let items = Object.keys(data)
            let currencies : Currency[] = items.map(key => {
                return {
                    code: key,
                    name: data[key]
                }
            })
            return currencies
        }


        /**
         * Retrieves the list of exchange rates
         */
        static async getExchangeRates(): Promise<ExchangeRates> {
            let url = 'data/latest.json'

            /*
            return fetch(url)
                    .then(response => response.json())
                    .then(data => {
                        let items = Object.keys(data["rates"])
                        let exchangeRates : ExchangeRates = {
                            baseCurrency: data["base"],
                            rates: items.map(key => {
                                return {
                                    currency: key,
                                    rate: data["rates"][key]
                                }
                            })
                        }
                        return exchangeRates
                    })
            */

            let response = await fetch(url)
            let data = await response.json()
            let items = Object.keys(data["rates"])
            let exchangeRates : ExchangeRates = {
                baseCurrency: data["base"],
                rates: items.map(key => {
                    return {
                        currency: key,
                        rate: data["rates"][key]
                    }
                })
            }
            return exchangeRates
        }

    }

}