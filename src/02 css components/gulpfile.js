var gulp = require('gulp');

var paths = {
    source: 'src/02 css components/',
    destination: 'dist/'
};


gulp.task('build', function() {
    gulp
        .src([
            paths.source + '**/*.html',
            paths.source + '**/*.css',
        ])
        .pipe(gulp.dest(paths.destination));
});