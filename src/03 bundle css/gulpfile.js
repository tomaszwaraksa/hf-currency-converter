var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var sequence = require('run-sequence');
var watch = require('gulp-watch');

var paths = {
    source: 'src/03 bundle css/',
    destination: 'dist/'
};

/**
 * Cleans dist folder
 */
gulp.task('clean', function() {
    return gulp
        .src(paths.destination, { read: false })
        .pipe(clean())
});


/**
 * Builds HTML files and moves to dist folder
 */
gulp.task('build-html', function() {
    gulp
        .src(paths.source + '**/*.html')
        .pipe(gulp.dest(paths.destination));
});


/**
 * Builds CSS files and moves to dist folder
 */
gulp.task('build-css', function() {
    gulp
        .src(paths.source + '**/*.css')
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(paths.destination + 'css/'));
});


/** Build the whole application */
gulp.task('build', ['build-html', 'build-css']);


/**
 * Watch file changes and rebuild the application
 */
gulp.task('watch', ['build'], function() {
   gulp.watch('src/**/*.*', ['build']);
});


