/// <reference path="../model/currency.ts" />
/// <reference path="../model/exchange-rate.ts" />

namespace HF.CurrencyConverter {

    /**
     * Currency converter
     */
    export class CurrencyConverter {

        /**
         * Converts the specified amount from source to target currency
         */
        convert(
            fromCurrency: string,
            toCurrency: string,
            amount: number,
            currencies: Currency[],
            exchangeRates: ExchangeRates): number
        {
            // clear errors
            this.error = undefined

            // validate parameters
            if (!fromCurrency || fromCurrency.trim() == '') {
                this.error = 'Source currency not specified'
                return
            }
            if (!toCurrency || toCurrency.trim() == '') {
                this.error = 'Target currency not specified'
                return
            }
            if (!currencies.find((currency: Currency) => currency.code == fromCurrency)) {
                this.error = 'Invalid currency: ' + fromCurrency
                return
            }
            if (!currencies.find((currency: Currency) => currency.code == toCurrency)) {
                this.error = 'Invalid currency: ' + toCurrency
                return
            }
            if (!amount || isNaN(amount) || amount < 0) {
                this.error = 'Invalid amount'
                return
            }

            // Convert
            // 1. Convert from source currency to base currency
            let exchangeRate = exchangeRates.rates.find(rate => rate.currency == fromCurrency)
            if (!exchangeRate) {
                this.error = 'Exchange rate for ' + fromCurrency + ' not found'
                return
            }
            let result = amount / exchangeRate.rate

            // 2. Convert from base currency to target currency
            exchangeRate = exchangeRates.rates.find(rate => rate.currency == toCurrency)
            if (!exchangeRate) {
                this.error = 'Exchange rate for ' + toCurrency + ' not found'
                return
            }
            result = result * exchangeRate.rate

            return result

        }


        /** Last error message */
        error: string
    }

}