/// <reference path="../model/currency.ts" />
/// <reference path="../model/exchange-rate.ts" />
/// <reference path="../model/configuration.ts" />

namespace HF.CurrencyConverter {

    export class APIClient {

        /**
         * Retrieves the list of currencies
         */
        static async getCurrencies(configuration: EnvironmentConfiguration): Promise<Currency[]> {
            // try cached version
            let currencies : Currency[]
            if (configuration.endpoints.allowCache) {
                try {
                    let cached = localStorage.getItem('currencies')
                    if (cached) {
                        currencies = <Currency[]>JSON.parse(cached)
                        if (currencies) {
                            console.log('Using cached currencies')
                        }
                    }
                }
                catch (error) {
                }
            }
            else {
                try {
                    localStorage.removeItem('currencies')
                }
                catch (error) {

                }
            }

            if (!currencies) {
                console.log('Fetching currencies from ', configuration.endpoints.currencies)
                let response = await fetch(configuration.endpoints.currencies)
                let data = await response.json()
                let items = Object.keys(data)
                currencies = items.map(key => {
                    return {
                        code: key,
                        name: data[key]
                    }
                })

                // cache in local storage
                if (configuration.endpoints.allowCache) {
                    try {
                        localStorage.setItem('currencies', JSON.stringify(currencies))
                        console.log('Currencies stored in cache')
                    }
                    catch (error) {
                    }
                }
            }

            return currencies
        }


        /**
         * Retrieves the list of exchange rates
         */
        static async getExchangeRates(configuration: EnvironmentConfiguration): Promise<ExchangeRates> {
            console.log('Fetching exchange rates from ', configuration.endpoints.exchangeRates)
            let response = await fetch(configuration.endpoints.exchangeRates)
            let data = await response.json()
            let items = Object.keys(data["rates"])
            let exchangeRates : ExchangeRates = {
                baseCurrency: data["base"],
                rates: items.map(key => {
                    return {
                        currency: key,
                        rate: data["rates"][key]
                    }
                })
            }
            return exchangeRates
        }

    }

}