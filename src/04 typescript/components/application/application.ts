namespace HF.CurrencyConverter {

    /**
     * Application controller
     */
    export class Application {

        /** Initializes the application */
        static main() {
            console.log('Ready.')
        }
    }


    // Initialize the application when window has finished rendering
    window.addEventListener('load', () => {
        Application.main()
    })

}