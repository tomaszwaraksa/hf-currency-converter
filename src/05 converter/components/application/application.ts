/// <reference path="../../model/currency.ts" />
/// <reference path="../../model/exchange-rate.ts" />
/// <reference path="../../components/form/form.ts" />

namespace HF.CurrencyConverter {

    /**
     * Application controller
     */
    export class Application {

        /**
         * List of supported currencies
         */
        static currencies: Currency[]


        /**
         * List of exchange rates
         */
        static exchangeRates: ExchangeRates


        /**
         * Form controller
         */
        static formController: FormController


        /** Initializes the application */
        static main() {

            // fetch currencies
            Application.currencies = [
                {
                    code: "USD",
                    name: "US Dollar"
                },
                {
                    code: "EUR",
                    name: "Euro"
                }
            ]

            // fetch exchange rates
            Application.exchangeRates = {
                baseCurrency: "USD",
                rates: [
                    {
                        currency: "USD",
                        rate: 1.00
                    },
                    {
                        currency: "EUR",
                        rate: 0.82
                    }
                ]
            }

            // initialize the input form
            Application.formController = new FormController(Application.currencies, Application.exchangeRates)

            console.log('Ready.')
        }
    }


    // Initialize the application when window has finished rendering
    window.addEventListener('load', () => {
        Application.main()
    })

}