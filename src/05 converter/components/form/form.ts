/// <reference path="../../model/currency.ts" />
/// <reference path="../../model/exchange-rate.ts" />
/// <reference path="../../services/currency-converter.ts" />


namespace HF.CurrencyConverter {

    export class FormController {

        constructor(
            public currencies: Currency[],
            public exchangeRates: ExchangeRates
        ) {
            // Get UI controls
            this.container = <HTMLElement>document.querySelector('#form-converter')
            this.fromCurrency = <HTMLInputElement>this.container.querySelector('#input-currency-from')
            this.toCurrency = <HTMLInputElement>this.container.querySelector('#input-currency-to')
            this.amount = <HTMLInputElement>this.container.querySelector('#input-amount')
            this.buttonConvert = <HTMLElement>this.container.querySelector('#button-convert')
            this.panelResult = <HTMLElement>this.container.querySelector('#panel-result')
            this.labelResult = <HTMLElement>this.container.querySelector('#label-result')
            this.panelError = <HTMLElement>this.container.querySelector('#panel-error')
            this.labelError = <HTMLElement>this.container.querySelector('#label-error')

            // Wire up events
            this.buttonConvert.addEventListener('click', () => {
                this.convert()
            })

            // Clear all
            this.clear()

            console.log('Form controller ready.')
        }


        /** Clears the form */
        private clear() {
            this.fromCurrency.value = 'EUR'
            this.toCurrency.value = 'USD'
            this.amount.value = '1.00'
            this.hide(this.panelResult)
            this.hide(this.panelError)
        }

        /**
         * Performs currency conversion
         */
        private convert() {
            this.hide(this.panelResult)
            this.hide(this.panelError)

            let converter = new CurrencyConverter()

            let result = converter.convert(
                this.fromCurrency.value.trim().toUpperCase(),
                this.toCurrency.value.trim().toUpperCase(),
                parseFloat(this.amount.value.trim()),
                this.currencies,
                this.exchangeRates
            )

            if (result) {
                this.show(this.panelResult)
                this.labelResult.innerText = result.toFixed(2) + ' ' + this.toCurrency.value
            }
            else {
                this.show(this.panelError)
                this.labelError.innerText = converter.error || 'Oops... Something went wrong. We apologize for any inconvenience!'
            }
        }


        /**
         * Hides an element
         */
        hide(element: HTMLElement) {
            element.style.display = "none"
        }


        /**
         * Displays an element
         */
        show(element: HTMLElement) {
            element.style.display = ""
        }


        /** HTML input elements */
        private container: HTMLElement
        private fromCurrency: HTMLInputElement
        private toCurrency: HTMLInputElement
        private amount: HTMLInputElement
        private buttonConvert: HTMLElement
        private panelResult: HTMLElement
        private labelResult: HTMLElement
        private panelError: HTMLElement
        private labelError: HTMLElement
    }

}