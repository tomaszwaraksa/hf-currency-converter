/// <reference path="../model/currency.ts" />
/// <reference path="../model/exchange-rate.ts" />

namespace HF.CurrencyConverter {

    /**
     * Currency converter
     */
    export class CurrencyConverter {

        /**
         * Converts the specified amount from source to target currency
         */
        convert(
            fromCurrency: string,
            toCurrency: string,
            amount: number,
            currencies: Currency[],
            rates: ExchangeRates): number
        {
            // clear errors
            this.error = undefined

            // validate parameters
            if (!fromCurrency || fromCurrency.trim() == '') {
                this.error = 'Source currency not specified'
                return
            }
            if (!toCurrency || toCurrency.trim() == '') {
                this.error = 'Target currency not specified'
                return
            }
            if (!currencies.find((currency: Currency) => currency.code == fromCurrency)) {
                this.error = 'Invalid currency: ' + fromCurrency
                return
            }
            if (!currencies.find((currency: Currency) => currency.code == toCurrency)) {
                this.error = 'Invalid currency: ' + toCurrency
                return
            }
            if (!amount || isNaN(amount) || amount < 0) {
                this.error = 'Invalid amount'
                return
            }

            // Convert
            // 1. Convert from source currency to base currency
            // 2. Convert from base currency to target currency
            return 123.11

        }


        /** Last error message */
        error: string
    }

}