namespace HF.CurrencyConverter {

    /**
     * Exchange rate
     */
    export interface ExchangeRate {

        /**
         * Currency code
         */
        currency: string

        /**
         * Amount of currency with value the same as 1 unit of base currency
         */
        rate: number

    }


    /**
     * Exchange rates
     */
    export interface ExchangeRates {
        /**
         * Base currency code
         */
        baseCurrency: string

        /**
         * Exchange rates using base currency
         */
        rates: ExchangeRate[]
    }

}