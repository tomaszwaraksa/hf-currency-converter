/// <reference path="../../model/currency.ts" />
/// <reference path="../../model/exchange-rate.ts" />
/// <reference path="../../model/configuration.ts" />
/// <reference path="../../components/form/form.ts" />
/// <reference path="../../services/configuration-provider.ts" />
/// <reference path="../../services/api-client.ts" />

namespace HF.CurrencyConverter {

    /**
     * Application controller
     */
    export class Application {

        /**
         * Application configuration
         */
        static configuration: EnvironmentConfiguration

        /**
         * List of supported currencies
         */
        static currencies: Currency[]


        /**
         * List of exchange rates
         */
        static exchangeRates: ExchangeRates


        /**
         * Form controller
         */
        static formController: FormController


        /** Initializes the application */
        static async main() {
            let form = document.querySelector('#form-converter')
            let loader = document.querySelector('#loader')
            DOM.hide(form)
            DOM.show(loader)

            // fetch configuration, currencies and exchange rates
            Application.configuration = await ConfigurationProvider.get()
            Application.currencies = await APIClient.getCurrencies(Application.configuration)
            Application.exchangeRates = await APIClient.getExchangeRates(Application.configuration)

            // initialize the input form
            Application.formController = new FormController(Application.currencies, Application.exchangeRates)

            DOM.hide(loader)
            DOM.show(form)
            console.log('Ready.', Application.configuration)
        }
    }


    // Initialize the application when window has finished rendering
    window.addEventListener('load', () => {
        Application.main()
    })

}