# Hackers and Founders Build a Currency Converter

Hands-on practical experiment in building an actually useful web application. We will build a currency conversion service, while introducing ourselves into the realm of Modern JavaScript and Front-End development.

The emphasis is on lean and minimalistic approach. We won't start with configuring WebPack, rather than that we'll just start building things. We'll make best use of standard functionality provided by browsers, while avoiding frameworks like plague. Our focus will be on sound architecture,good programming practices, readability and extensibility.


## The Task
### Functionality
* Simple currency converter, allowing to enter amount and source currency, then converting it to target currency
* Use live data from publicly available APIs - currency list, currency exchange rates
* Use geo services to suggest the currency applicable to actual location of the user

### User interface
* Modular design following BEM principles and CSS components
* Responsive design
* Proper separation of concerns with application layers (model, UI controllers, application services)

### Technology
* Build the application using gulp
* Use TypeScript
* Use modern HTML5 APIs instead of frameworks
* Take care of less capable browsers with polyfills
* Allow prototyping without live APIs, using mock data
* Use local storage to cache the data which doesn't change often (list of currencies)


# GET CODE

    git clone https://bitbucket.org/tomaszwaraksa/hf-currency-converter.git
    cd hf-currency-converter
    npm install
    npm install live-server -g


# FIRST STEPS

## Lets look at the APIs
Test tool: Postman at https://www.getpostman.com/

### Currency exchange rates

* API provider: https://openexchangerates.org/
* App ID: 04ac49916b414c03b3977a985a8ca127 (get your own!)
* All currencies: https://openexchangerates.org/api/currencies.json?app_id=04ac49916b414c03b3977a985a8ca127
* Currency rates: https://openexchangerates.org/api/latest.json?app_id=04ac49916b414c03b3977a985a8ca127
* Unfortunately /convert API is only available on paid plans ...

### Geo-information
* http://www.geonames.org/
* free access, account has to be created though
* Example: http://ws.geonames.org/countryCode?lat=54.12&lng=9.2&type=JSON&username=tomaszwaraksa

### Country information
* https://restcountries.eu/
* Get country details by code, for example DE: https://restcountries.eu/rest/v2/alpha/DE
* free access

## Create folder structure

    mkdir hf-currency-converter
    cd hf-currency-converter
    mkdir docs src dist

## Introduce npm
Let's introduce npm to our project, to easily download dependencies, run scripts etc.

    npm init



# 00 PROTOTYPE

## PROTOTYPE 1 - skeleton

* Introduce Visual Studio Code IDE - but only because I like it. Any other code editor of your preference will do just as well
* Create rough HTML skeleton
* Introduce live-server - For easy testing our application locally, without the need to deploy it to web server. You're free to use Apache though, nothing wrong with that. Live Server is a simple NodeJS application which works as a server for static files. The simplest server possible, requiring zero configuration (unless you do need it). And it auto-reloads your pages in the browsers, as you type your code!

    npm install live-server ---save-dev

It's so good, that we recommend installing it globally as well:

    npm install live-server -g


## PROTOTYPE 2 - CSS hierarchy
Following BEM principles

## PROTOTYPE 3 - CSS styles
* Use Google OpenSans font at https://fonts.googleapis.com/css?family=Open+Sans:300,400,700
* Use color palette at https://color.adobe.com/Essentials-of-Facilitation-color-theme-9597098/edit/?copy=true
* Define CSS styles for the previously designed CSS hierarchy

## PROTOTYPE 4 - Responsive design
* Introduce Browser Developer Tools
* Show how to use tools for mobile design, quick look at DOM and CSS inspection
* Define styles for small screens

## FINAL - move it to /src



# BUILDING AND DEPLOYMENT

## src vs dist
* Explain why
* Introduce gulp - in commercial projects you will use some sort of a build pipeline. Sooner or later, some repetitive tasks will emerge. You will then need a tool to automate such tasks. Our usage of gulp here will be minimalistic, limited to the following:

    * bundling of CSS assets
    * triggering TypeScript compiler, so that your code is compiled on the fly
    * collecting all bits and pieces of your web app into /dist folder

To install it, run

    npm install gulp --save-dev

> THIS IS NOT A GULP COURSE, BY THE WAY!!!

## 01 SIMPLE
Create simple gulp script which copies files from /src to /dist

## 02 CSS COMPONENTS
Split the styles into separate files, representing visual components

* create folders for components: application, header, form, footer
* split CSS into chunks, per component, mobile separately
* modify gulp script to copy all CSS, using globs **
* modify index.html to link the components

## 03 BUNDLE CSS
* modify gulp file:
  * introduce gulp-clean, purge /dist before everything else is done
  * introduce gulp-concat, bundle css into styles.css
  * introduce composite build - combination of clean, build html and build css
  * explain gulp trouble with parallel execution of tasks, introduce run-sequence
* modify index.html again
* introduce gulp-watch



# APPLICATION LOGIC

## 04 TYPESCRIPT
We will use TypeScript to introduce modern dialect of JavaScript in our application. TS code will be compiled to pure JavaScript (ECMAScript 5 flavour, which runs on pretty much anything).

    npm install typescript --save-dev


* application/application.ts - simple static class with main function, called on window.load
* compile .ts files using gulp
* add to index.html



## 05 Implement currency converter
Lets start implementing application logic using mock data, before we start using the actual API.

* introduce model classes - we'd rather work on internal model, this allows switching to another provider!
* introduce currency-converter service
* introduce form controller, which will handle UI events and updates
* link things up in application

### POLYFILLS
We need to introduce es6 polyfill now. In CurrencyConverter class we want to use Array.find method, which is only available in ES6 standard. Older browsers (including ALL versions of Internet Explorer) will just throw on this command. See https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/find

    npm install es6-shim

We also need to include this in our gulp build process. We will bundle all third-party libraries into libraries.js and deploy it separately, for easier debugging.

Now we can safely change TypeScript compiler settings in gulp script, so that it compiles to ES6 dialect. Notice the reduction in size of the generated JS bundle! ES6 dialect is simply less verbose.

If we want to be super-super backwards compatible, we should also provide es-5 polyfill, which will help Internet Explorer 9 and some obsolete mobile phones as well.

    npm install es5-shim



## 06 Use mock data
Lets start implementing application logic using mock data, before we start using the actual API.

* create data files under data: currencies.json and latest.json, taken from OpenExchangeRates.org
* add task to gulp, so that data files are deployed as well
* introduce api-client service
* in api-client implement fetching data using mock data files:
    - no more JQuery just for the sake of AJAX request!
    - excellent article about fetch: https://davidwalsh.name/fetch
    - install fetch polyfill from whatwg-fetch, add to build process!
* add code in application service to fetch currencies and exchange rates
* replace promise chains with async
* implement actual conversion code in CurrencyConverter class




## 07 Connect to API, use Local Storage
Let's start using the actual OpenExchange API, along with Local Storage. Objectives:

* real exchange rates from the API used
* we need configuration, a place to store App ID
* store currencies in Local Storage, after all they're not changing that often
* keep ability to use mock data, again by using configuration - introduce the concept of environment
* introduce application loader component
* introduct DOM service - manipulating CSS in JS is a no-no, better have that addClass/RemoveClass utility
  Replace hiding in form.ts with addClass/removeClass
* show local storage content using developer tools


## 08 Geolocation services
Task:
    - identify where we are
    - determine country
    - determine currency of that country
    - populate the fromCurrency box with it!
    - and we could even translate the UI

* APIs
  - https://developer.mozilla.org/en-US/docs/Web/API/Geolocation
  - http://ws.geonames.org/countryCode?lat=54.12&lng=9.2&type=JSON&username=tomaszwaraksa
  - https://restcountries.eu/rest/v2/alpha/DE

* add /services/geo.ts
    - determine geo-location using HTML5 Geo API
    - determine country at the returned location using GeoNames API
    - determine country currency using RestCountries API
    - initialize form with the retrieved currency


### AND THAT'S IT
Anything beyond that should be added with utmost care and consideration, only if you are really sure that it's going to save you major efforts in future. Just remember that writing 50 lines of code yourself, rather than pulling yet another library from npm, IS NOT CONSIDERED HERE a major effort. Am I a programmer or not, after all?

# Nice-to-haves
Good base, but a couple of concerns would have to be addressed before we can consider this a decent application:

* unit tests
* translations, using geolocation / browser preferences / user preference
* some sort of component framework - the UI would be even better if split into HTML components
* error handling
* security, input and API results sanitization
