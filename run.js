'use strict';
let fs = require('fs')
let path = require('path')
let childProcess = require('child_process')
let liveServer = require("live-server")


// Script for compiling and running the specified phase of the project
let phases = [
    '00 prototype',
    '01 simple',
    '02 css components',
    '03 bundle',
    '04 typescript',
    '05 converter',
    '06 mock data',
    '07 api',
    '08 geolocation'
]

// phase to run
let phase = phases.find(name => name.startsWith(process.argv[2]))
if (phase) {
    console.log('Running project phase [' + phase.toUpperCase() + ']')

    // If gulp script exists, copy to root, run and launch app from /dist
    let gulpScriptPath = path.join(__dirname, 'src', phase, 'gulpfile.js')
    if (fs.existsSync(gulpScriptPath)) {
        // copy gulp script
        let gulpScript = fs.readFileSync(gulpScriptPath, 'utf-8')
        fs.writeFileSync(path.join(__dirname, 'gulpfile.js'), gulpScript)

        // build the app
        childProcess.exec('gulp watch')

        // start live server from dist
        liveServer.start({
            open: '/dist/'
        })
    }
    else {
        // otherwise run directly from the phase folder
        liveServer.start({
            open: '/src/' + phase + '/'
        })
    }
}
else {
    console.log('Specify phase to run as script parameter, for example')
    console.log('')
    console.log('node run 02')
    console.log('')
}

